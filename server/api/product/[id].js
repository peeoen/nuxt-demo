export default defineEventHandler(async (event) => {
    const { id } = event.context.params;
  
    const { appId } = useRuntimeConfig();
    console.log(appId);
    console.log(id);
    const url = "https://fakestoreapi.com/products/" + id;
  
    const data = await $fetch(url)
    return data
  });
  